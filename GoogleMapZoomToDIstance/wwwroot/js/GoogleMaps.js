﻿class GoogleMaps {


	constructor(container) {
		this.MapContainer = document.querySelector(container);
		this.StartingPosition = {
			lat: 85,
			lng: 0
		}

		this.result = [];
	}

	async init() {
		console.log("GoogleMaps :: init :: 1");
		this.Map = await this.renderMap();
		console.log("GoogleMaps :: init :: 2");

		//disableLog();
	}
	async run() {

		this.Markers = this.addMarkers();
		console.log("GoogleMaps :: run :: 1");
		//await this.Loop();
		console.log("GoogleMaps :: run :: 2");

		console.log(this.Map.getProjection());
	
	}
	async renderMap() {
		return new google.maps.Map(this.MapContainer, {
			center: { lat: this.StartingPosition.lat, lng: this.StartingPosition.lng },
			zoom: 5
		});
	}

	async Loop() {
		for (var lat = this.StartingPosition.lat; lat >= 0 - this.StartingPosition.lat; lat--) {
			await this.setCenter(lat, this.StartingPosition.lng);
			for (var zoom = 3; zoom <= 19; zoom++) {
				await this.SetZoom(zoom);
				await this.moveMarkers(this.Markers, lat, zoom);
				var dis = await this.GetPixels(zoom);

				this.result.push([lat, zoom, '"' + dis + '"'].join(';'));
			}
		}
		console.log("done", this.result.join("\n"));
		return true;
	}

	async GetPixels(zoom) {
		let p1 = this.Map.getProjection().fromLatLngToPoint(this.Markers.marker1.getPosition());
		let p2 = this.Map.getProjection().fromLatLngToPoint(this.Markers.marker2.getPosition());
		
		let pixelSize = Math.pow(2, - this.Map.getZoom());

		let pxdistance = Math.sqrt((p1.x - p2.x) * (p1.x - p2.x) + (p1.y - p2.y) * (p1.y - p2.y)) / pixelSize;

		let pixelsPrM = pxdistance / distance[zoom]
		return pixelsPrM;
	}

	async setCenter(lat, lng) {
		this.Map.setCenter(new google.maps.LatLng(lat, lng));

		return true;
	}

	async SetZoom(zoom) {
		this.Map.setZoom(zoom);

		return true;
	}

	async moveMarkers(markers, lat, zoom) {

		var calcedLng = await this.clacLng(zoom, lat);

		markers.marker1.setPosition(new google.maps.LatLng(lat, this.StartingPosition.lng));
		markers.marker2.setPosition(new google.maps.LatLng(lat, calcedLng));

		return true;
	}

	addMarkers() {
		var marker1 = new google.maps.Marker({
			position: new google.maps.LatLng(this.StartingPosition.lat, this.StartingPosition.lng),
			map: this.Map
		});

		var marker2 = new google.maps.Marker({
			position: new google.maps.LatLng(this.StartingPosition.lat, this.clacLng(3, this.StartingPosition.lng)),
			map: this.Map
		});

		return { marker1, marker2 };
	}

	async clacLng(zoom, lat) {

		const kmInOneDegree = 111.32 //for google maps
		const degreeForOneKm = 1 / kmInOneDegree;
		const degreeForOneM = degreeForOneKm / 1000;

		var degreesInZoomDistance = distance[zoom] * degreeForOneM;

		var converter = Math.PI / 180; //to use 180 insted of 360
		var newLng = this.StartingPosition.lng + degreesInZoomDistance / Math.cos(lat * converter)

		return newLng;
	}
}
var distance = {
	3: 1000 * 500,
	4: 1000 * 500,
	5: 1000 * 500,
	6: 1000 * 500,
	7: 1000 * 500,
	8: 1000 * 500,
	9: 1000 * 500,
	10: 1000 * 500,
	11: 1000 * 500,
	12: 1000 * 500,
	13: 1000 * 500,
	14: 1000 * 500,
	15: 1000 * 500,
	16: 1000 * 500,
	17: 1000 * 500,
	18: 1000 * 500,
	19: 1000 * 500,
}
function Wait(time) {
	return new Promise(resolve => {
		setTimeout(c => {
			resolve(true);
		}, time)
	})
}

function disableLog() {
	console.log = () => { return; }
}